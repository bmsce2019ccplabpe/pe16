#include <stdio.h>
#include <string.h>

struct employee
{
	char name[20], id[20];
	int age, sal;
};

typedef struct employee empl;

empl input (int z, empl e[])
{
	for (int i = 0; i < z; i++) 
	{
	    printf("Please enter the employee details for employee %d\n" ,i);	
	    printf("Enter name of the employee: ");
	    gets(e[i].name);
	    gets(e[i].name);
	    printf("Enter ID no of the employee: ");
	    gets(e[i].id);
	    printf("Enter age of the employee: ");
	    scanf("%d",&e[i].age);
	    printf("Enter salary of the employee: ");
	    scanf("%d",&e[i].sal);
	 }
}

empl output (int z, empl e[])
{
	for (int i = 0; i < z; i++) 
	{
	    printf("Details of employee %d are \n" ,i);
	    printf("Name: \t ");
	    puts(e[i].name);
	    printf("ID : \t %s \n" ,e[i].id);
	    printf("Age: \t %d \n" ,e[i].age);
	    printf("Salary \t %d \n" ,e[i].sal);
	  
	}
}

char decide()
{
    char c,d;
    printf("Do you wish to view the Details of employees? [Y/N]");
    scanf("%c%c", &c ,&d);
    return d;
}
 

void main ()
{
	int a , z,buf[100];
	printf(" No of employees to be entered:");
	scanf("%d",&z);
	empl e[z];
	input (z, e);
	char c;
	loopstart :
    c = decide();
    switch(c)
    {
        case 'Y':output(z, e); break;
        case 'N': break;        
        default: goto loopstart; 
    }
}