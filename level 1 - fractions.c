#include  <stdio.h>

struct frac
{
	int nr;
	int dr;
};

typedef struct frac fra;

int hcf(int m, int n)
{
    int i,z;
    for (i=1; i<=m || i<=n; ++i)
    {    if (m%i==0 && n%i==0)
            z = i;
    }
    return z;
}
                

fra input ()
{
	fra a;
	printf("Enter the fraction number in format a/b\n");
	scanf("%d/%d" ,&a.nr , &a.dr);
	return a;
}

fra compute(fra a, fra b)
{
	fra g ,f;
    int x;
	f.nr = (a.nr*b.dr + a.dr*b.nr);
	f.dr = ( a.dr*b.dr );
    x = hcf(f.nr, f.dr);
    g.nr = f.nr/x;
    g.dr = f.dr/x;
	return g;
}

void main ()
{
	fra a ,b ,c;
    a = input ();
	b = input ();
	c = compute(a,b);
	printf("%d/%d" ,c.nr , c.dr);
}