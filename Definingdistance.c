#include <stdio.h>
#include <math.h>

float dis(float x1, float y1, float x2,  float y2)
{ 
    float z;
    z=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
return z;
}

int main()
{ 
    float a,b,c,d,e;
    printf("Enter the 1st co-oridinates\n");
    scanf("%f%f",&a,&b);
    printf("Enter the 2nd co-oridinates\n");
    scanf("%f%f", &c,&d);
    e = dis(a,b,c,d);
    printf("The distance between (%f,%f) and (%f,%f) is %f",a,b,c,d,e);
return 0;
    }
    
    
/* Line 4 float name(parameters); this name is used to call the main fucntion back in the program as shown in line 16
   Line 5 Start of user defined function
   Line 6 assaigning a varible to return to the program
   Line 7 Returning the variable back to the defined function
   Line 9 End of user defined function
   Line 11 Start of the main program
   Line 13 Declare variables of the program
   Line 15 Syntax scanf("%f",&x); [spaces don't matter; comma and & imp]
   Line 18 variables=functionname(varible in same order as defined in line 4) */